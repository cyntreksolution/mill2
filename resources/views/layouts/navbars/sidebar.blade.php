<div class="sidebar">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="#" class="simple-text logo-mini">{{ _('HRM') }}</a>
            <a href="#" class="simple-text logo-normal">{{ _('Hasinidu Rice Mill') }}</a>
        </div>
        <ul class="nav">
            <li @if ($pageSlug == 'dashboard') class="active " @endif>
                <a href="{{ route('home') }}">
                    <i class="tim-icons icon-chart-pie-36"></i>
                    <p>{{ _('Dashboard') }}</p>
                </a>
            </li>

            <li @if ($pageSlug == 'make-invoice') class="active " @endif>
                <a href="{{ route('invoice.create') }}">
                    <i class="tim-icons icon-notes"></i>
                    <p>{{ _('Make Invoice') }}</p>
                </a>
            </li>

            <li @if ($pageSlug == 'invoices') class="active " @endif>
                <a href="{{ route('invoice.index') }}">
                    <i class="tim-icons icon-bullet-list-67"></i>
                    <p>{{ _('Invoices') }}</p>
                </a>
            </li>

            <li @if ($pageSlug == 'items') class="active " @endif>
                <a href="{{ route('item.index') }}">
                    <i class="tim-icons icon-cart"></i>
                    <p>{{ _('Items') }}</p>
                </a>
            </li>

            <li @if ($pageSlug == 'product-category') class="active " @endif>
                <a href="{{ route('product-category.index') }}">
                    <i class="tim-icons icon-basket-simple"></i>
                    <p>{{ _('Products Category') }}</p>
                </a>
            </li>


            <li @if ($pageSlug == 'clients') class="active " @endif>
                <a href="{{ route('client.index') }}">
                    <i class="tim-icons icon-satisfied"></i>
                    <p>{{ _('Clients') }}</p>
                </a>
            </li>

            <li @if ($pageSlug == 'rice-warehouses') class="active " @endif>
                <a href="{{ route('rice.warehouse',['type'=>'rice']) }}">
                    <i class="tim-icons icon-app"></i>
                    <p>{{ _('Rice Warehouses') }}</p>
                </a>
            </li>
            <li @if ($pageSlug == 'stockpile') class="active " @endif>
                <a href="{{ route('rice.warehouse',['type'=>'paddy']) }}">
                    <i class="tim-icons icon-app"></i>
                    <p>{{ _('Stockpiles') }}</p>
                </a>
            </li>

            <li @if ($pageSlug == 'send-rice-to-stockpile') class="active " @endif>
                <a href="{{ route('rice.load') }}">
                    <i class="tim-icons icon-user-run"></i>
                    <p>{{ _('Load') }}</p>
                </a>
            </li>

        </ul>
    </div>
</div>
