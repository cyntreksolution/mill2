<footer class="footer">
    <div class="container-fluid">
        <div class="copyright">
            &copy; {{ now()->year }} {{ _('powered ') }} <i class="tim-icons icon-heart-2"></i> {{ _('by') }}
            <a href="http://cyntrek.com" target="_blank">{{ _('Cyntrek Solutions') }}</a>
        </div>
    </div>
</footer>
