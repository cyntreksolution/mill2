@extends('layouts.app', ['page' => __('Rice Warehouses'), 'pageSlug' => 'rice-warehouses'])

@section('content')
   <rice-warehouse-component></rice-warehouse-component>
@endsection

@push('js')

@endpush
