@extends('layouts.app', ['page' => __('Stockpiles'), 'pageSlug' => 'stockpile'])

@section('content')
   <stockpile-index-component></stockpile-index-component>
@endsection

@push('js')

@endpush
