<html>
<head>
    <link href="{{asset('/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('/css/invoice.css')}}" rel="stylesheet">
</head>
<body>
    <div class="row">
        <h5 class="inv">
            INVOICE
        </h5>
    </div>

    <div class="row">
        <div class="col-xs-8">
            @if($invoice->store_id == 1)
                <h2>
                    Hasinindu Rice Mill <br>
                </h2>
                <h4>
                    No.479 <br>
                    Debarawewa<br>
                    Thissamaharamaya<br>
                    047 223 7130/047 223 8130
                </h4>
            @else
               <h2>
                    Hasinindu Steam Rice Mill<br>
               </h2>
                <h4>
                    Raja Gedara <br>
                    Seedawaththa<br>
                    Weerawila<br>
                    047 223 7130/047 223 8130
                </h4>
            @endif
        </div>
        {{--<br>--}}
        <div class="col-sx-4">
            <h5>Billed To :<br>{{$invoice->client->name}}<br>{!! str_replace(',', '<br>', $invoice->client->address) !!}</h5>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="text-left col-xs-2">
            <h5>INVOICE NUMBER</h5>
            <h5>DATE OF ISSUE</h5>
            {{--<h5>DUE DATE : {{$item->due_date}}</h5>--}}
        </div>
        <div class="text-left col-xs-2">
            <h5>:{{$invoice->invoice_number}}</h5>
            <h5>:{{$invoice->invoice_date}}</h5>
            {{--<h5>DUE DATE : {{$item->due_date}}</h5>--}}
        </div>
        <div class="col-xs-4 pull-right text-right">
            <h5>AMOUNT DUE (LKR)</h5>
            <h4>{{number_format( (float) $invoice->total, 2, '.', ',')}}</h4>
            {{--<h5>DUE DATE : {{$item->due_date}}</h5>--}}
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12">
            <table class="table" style="margin-top: 50px;">
                <thead class="thead-inverse">
                <tr style="background-color: #656872; color: white">
                    <th>#</th>
                    <th>Item</th>
                    <th class="text-center">Qty.</th>
                    <th class="text-center">Unit value(LKR)</th>
                    <th class="text-right">Line Total (LKR)</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($invoice->items as $key => $item)
                        <tr>
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$item->product->name.' '.$item->product->variant->item->name.' '.$item->product->variant->size.' KG'}}</td>
                            <td class="text-center">{{$item->quantity}}</td>
                            <td class="text-center">{{number_format( (float) sprintf('%0.2f', $item->rate), 2, '.', ',')}}</td>
                            <td class="text-right">{{number_format( (float) $item->total, 2, '.', ',')}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>

    <div class="col-xs-4 pull-right text-right">
        <h4> Amount Due (LKR):{{number_format( (float) $invoice->total, 2, '.', ',')}}</h4>
    </div>

    <div class="row" style="margin-top: 80px;">
        <div class="col-xs-3">
            <h5>
                .........................................<br>
                Signature-Authorized
            </h5>
        </div>


        <div class="col-xs-3 text-right pull-right">
            <h5>
                .........................................<br>
                Signature-Received
            </h5>
        </div>

    </div>
    <footer>
        <div class="row" style="margin-top: 20px;">
            <h4 align="center">
                <h6 class="text-muted">powered by cyntrek</h6>
            </h4>

        </div>
    </footer>
</body>

</html>
