@extends('layouts.app', ['page' => __('Invoices'), 'pageSlug' => 'invoices'])

@section('content')
   <invoice-table-component></invoice-table-component>
@endsection

@push('js')

@endpush
