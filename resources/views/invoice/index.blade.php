@extends('layouts.app', ['page' => __('Generate Invoices'), 'pageSlug' => 'make-invoice'])

@section('content')
    <invoice-index-component></invoice-index-component>
@endsection

@push('js')

@endpush
