@extends('layouts.app', ['page' => __('Items'), 'pageSlug' => 'items'])

@section('content')
    <item-index-component></item-index-component>
@endsection

@push('js')

@endpush
