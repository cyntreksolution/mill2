@extends('layouts.app', ['page' => __('Products'), 'pageSlug' => 'product-category'])

@section('content')
    <product-index-component></product-index-component>
@endsection

@push('js')

@endpush
