@extends('layouts.app', ['page' => __('Send Packed Rice To Stockpile'), 'pageSlug' => 'send-rice-to-stockpile'])

@section('content')
    <rice-load-component></rice-load-component>
@endsection

@push('js')

@endpush
