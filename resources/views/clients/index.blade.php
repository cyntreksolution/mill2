@extends('layouts.app', ['page' => __('Clients'), 'pageSlug' => 'clients'])

@section('content')
    <clients-index-component></clients-index-component>
@endsection

@push('js')

@endpush
