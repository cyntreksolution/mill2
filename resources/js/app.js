/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.axios = require('axios');


import vSelect from 'vue-select';
window.Swal =require('sweetalert2');
import Multiselect from 'vue-multiselect';
Vue.component('multiselect', Multiselect);
Vue.component('v-select', vSelect);
Vue.component('pagination', require('laravel-vue-pagination'));
let secret ='eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImNlMGJhNDU4YWFjNTEyZGRkOTNlMTk5OTMyMTM0NDE0NGNkYTFmNmRmMmJlNmJkN2I3ZTM4ZDFjMzQ2ZmYyMjI2YmZjYTRmZDUyYzVkNTUwIn0.eyJhdWQiOiI0IiwianRpIjoiY2UwYmE0NThhYWM1MTJkZGQ5M2UxOTk5MzIxMzQ0MTQ0Y2RhMWY2ZGYyYmU2YmQ3YjdlMzhkMWMzNDZmZjIyMjZiZmNhNGZkNTJjNWQ1NTAiLCJpYXQiOjE1NjkzMDU1OTAsIm5iZiI6MTU2OTMwNTU5MCwiZXhwIjoxNjAwOTI3OTkwLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.IBvLPErq3f0vRC54YYuJBTOxsX8K1Ir4DEl85KaP9SyYzaoStka8be3WksC0MzApI4161HrPZD4tgnEMIrzJn4FR7nKbNPj7bP2WJ00bUpMao70oQbuCrzz9-4ZBT8W9_BJrMnehyfEMJyQdtDehQwXWVii0HLSpZ0xNKkVwMaTx35HvJKQtHiqe70X37a1-eAaju-jV1MuFT2Jkepu23X0Sd0tAs2ou8gwPghSgu17uFbQdZKJARfvgGwfktUrzLrQTIZ6Zlfl--IYfE7Pju1kL9vgD5rzWa9ihxB5ym7Sbtk09wnaP2kUatXbTxpDxLdQsfXb8oRF6zBFCmUIdT9Q4BQDYPuK6FAZgm9HxKElkWw5IIrIK2gAp789L-rC-KGL9dNK7qf2H1Yhi1REStV0sGFao465BH8Rq2FKAdVloCRgSQh306xY0AH7DWbzBlh0r7Y6yjgOZ414RDmTgfu2YWpgqLEwyKuP4rJsqk54Wu-kRe2CpE6EPTjPzG_ReBN0mGLyBU1AOUfSkAn64TZnf0nCPJG8kArn9ucc5yOeO84Mvm-bTrusIeMRW4hEYr5c30YmlPdyRk2yEIrIqqonAsxmXEq9YL9eRkP1EnwXSKkF8dv7nmxD7LcLKIZ0Xc5BoNFUg5LI8yO0olxncL51rAd5Se708nNeavpCQhKg';
axios.defaults.headers.common['Authorization'] = "Bearer " + secret;


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('item-index-component', require('./components/items/ItemIndexComponent').default);
Vue.component('item-variants-component', require('./components/items/ItemVariantsComponent').default);
Vue.component('category-product-component', require('./components/products/CategoryProductComponent').default);
Vue.component('product-index-component', require('./components/products/ProductIndexComponent').default);
Vue.component('clients-index-component', require('./components/clients/ClientsIndexComponent').default);
Vue.component('stockpile-index-component', require('./components/stockpiles/StockpileIndexComponent').default);
Vue.component('intake-stockpile-component', require('./components/stockpiles/IntakeStockpileComponent').default);
Vue.component('rice-load-component', require('./components/products/RiceLoadComponent').default);
Vue.component('rice-load-table-component', require('./components/products/RiceLoadTableComponent').default);
Vue.component('invoice-index-component', require('./components/invoice/InvoiceIndexComponent').default);
Vue.component('clients-details-component', require('./components/invoice/ClientsDetailsComponent').default);
Vue.component('my-atom-spinner', require('./components/MyAtomSpinner').default);
Vue.component('invoice-table-component', require('./components/invoice/InvoiceTableComponent').default);
Vue.component('rice-warehouse-component', require('./components/stockpiles/RiceWarehouseComponent').default);
Vue.component('customers-widget-component', require('./components/dashboard/CustomersWidgetComponent').default);
Vue.component('invoice-widget-component', require('./components/dashboard/InvoiceWidgetComponent').default);
Vue.component('sales-widget-component', require('./components/dashboard/SalesWidgetComponent').default);
Vue.component('vehicle-widget-component', require('./components/dashboard/VehicleWidgetComponent').default);



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
