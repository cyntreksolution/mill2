notification = {
    show: function (type, title, message, showProgressbar = false, url = null) {
        let icons = {
            "info": "tim-icons icon-check-2",
            "warning": "tim-icons icon-bell-55",
            "success": "tim-icons icon-bulb-63",
            "danger": "tim-icons icon-alert-circle-exc",
        };

        $.notify({
            // options
            icon: icons[type],
            title: title,
            message: message,
            url: url,
            target: '_blank'
        }, {
            // settings
            element: 'body',
            position: null,
            type: type,
            allow_dismiss: true,
            newest_on_top: false,
            showProgressbar: showProgressbar,
            placement: {
                from: "top",
                align: "right"
            },
            offset: 20,
            spacing: 10,
            z_index: 1100,
            delay: 5000,
            timer: 10000,
            url_target: '_blank',
            mouse_over: null,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            onShow: null,
            onShown: null,
            onClose: null,
            onClosed: null,
            icon_type: 'class',
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}"  role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
    },

    showSuccessMessage: function (message) {
        this.show("success", "Wow !", message);
    },
    showErrorMessage: function (message) {
        this.show("danger", "Sorry !", message);
    },
    showInfoMessage: function (message) {
        this.show("info", "Nice !", message);
    },
    showWarningMessage: function (message) {
        this.show("warning", "Alert !", message);
    },

    showDeleteDialogMessage: function (url) {
        Swal.fire({
            title: 'Are you sure? Do You want to Delete ?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                let data;
                axios.delete(url)
                    .then(function (response) {
                    let data = response.data;
                    if (data.status == 'success') {
                        Swal.fire(
                            'Deleted!',
                            data.message,
                            'success'
                        );
                        return true;
                    } else {
                        Swal.fire(
                            'Sorry !',
                            'Something went wrong !',
                            'error'
                        )
                        return false;
                    }
                }).catch(function (error) {
                    Swal.fire(
                        'Sorry !',
                        'Something went wrong !',
                        'error'
                    )
                    return false;
                })
            }
        })
    }
};
