<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::resource('items', 'ItemController', ['except' => ['create','edit']]);
    Route::resource('variants', 'ItemVariantController', ['except' => ['create','edit']]);
    Route::resource('category', 'ProductCategoryController', ['except' => ['create','edit']]);
    Route::resource('client', 'ClientController', ['except' => ['create','edit']]);
    Route::resource('invoice', 'InvoiceController', ['except' => ['create','edit']]);

    Route::get('category/{category}/available-items', 'ProductCategoryController@availableItems')->name('category.available');
    Route::get('all/client/{client}','ClientController@show')->name('clients.show');
    Route::post('category/{category}/add-item', 'ProductController@store')->name('product.store');

    Route::get('all/category/{paginate?}', 'ProductCategoryController@getAllCategories')->name('category.all');
    Route::get('all/items/{paginate?}', 'ItemController@getAllProducts')->name('products.all');
    Route::get('all/products/{paginate?}', 'ProductController@getAllProducts')->name('products.all');
    Route::get('available/products/{paginate?}', 'ProductController@availableProducts')->name('products.available');
    Route::get('all/clients','ClientController@getAllClients')->name('clients.all');
    Route::get('all/drivers','DriverController@getAllDrivers')->name('drivers.all');
    Route::get('all/stockpiles','StockpileController@getAllStockpiles')->name('stockpile.all');
    Route::get('all/rice/stockpiles/{paginate?}','StockpileController@getAllRiceStockpiles')->name('stockpile.rice');
    Route::get('all/intake-stockpiles','IntakeStockpileController@getAllIntakeStockpiles')->name('stockpile.all');
    Route::get('all/rice/load/{paginate?}','StockTransactionController@getRecentRiceLoading')->name('riceload.all');
    Route::get('all/invoices','InvoiceController@getAllInvoices')->name('invoice.all');

    Route::post('load/products/{type}', 'ProductController@loadProducts')->name('products.all');


    Route::get('widget/sales','HomeController@getMonthlySales')->name('widget.sales');

});
