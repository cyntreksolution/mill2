<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/auth/passport', function () {
    return view('debug');
});

Auth::routes();
Route::get('/', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
		Route::get('icons', ['as' => 'pages.icons', 'uses' => 'PageController@icons']);
		Route::get('maps', ['as' => 'pages.maps', 'uses' => 'PageController@maps']);
		Route::get('notifications', ['as' => 'pages.notifications', 'uses' => 'PageController@notifications']);
		Route::get('rtl', ['as' => 'pages.rtl', 'uses' => 'PageController@rtl']);
		Route::get('tables', ['as' => 'pages.tables', 'uses' => 'PageController@tables']);
		Route::get('typography', ['as' => 'pages.typography', 'uses' => 'PageController@typography']);
		Route::get('upgrade', ['as' => 'pages.upgrade', 'uses' => 'PageController@upgrade']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

    Route::resource('item', 'ItemController', ['only' => ['index','create','edit']]);
    Route::resource('product-category', 'ProductCategoryController', ['only' => ['index','create','edit']]);
    Route::resource('client', 'ClientController', ['only' => ['index','create','edit']]);
    Route::resource('stockpile', 'StockpileController', ['only' => ['create','edit']]);
    Route::resource('invoice', 'InvoiceController',['only' => ['index','create','edit']]);

    Route::get('stockpile/{type}/warehouse', 'IntakeStockpileController@index')->name('rice.warehouse');
    Route::get('product/rice/load', 'ProductController@sendRiceView')->name('rice.load');
    Route::get('invoice/{filename}/{action}', 'InvoiceController@pdfRequestLink')->name('invoice.action');

});

Route::get('/debug','DebugController@index');

