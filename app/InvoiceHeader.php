<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceHeader extends Model
{
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function items(){
        return $this->hasMany(InvoiceData::class,'invoice_header_id');
    }

    public static function invoiceNo()
    {
        $in = InvoiceHeader::max('id') + 1;
        return $invoiceNo = date('ymd') . '' . $in;
    }
}
