<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntakeStockpile extends Model
{
    public function stockpile(){
        return $this->belongsTo(Stockpile::class);
    }
}
