<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    protected $appends = ['product_name'];
    public function category(){
        return $this->belongsTo(ProductCategory::class,'product_category_id');
    }

    public function variant(){
        return $this->belongsTo(ItemVariant::class,'item_variant_id');
    }

    public function bag(){
        return $this->hasOne(Bag::class);
    }

    public function transactions(){
        return $this->hasMany(StockTransaction::class);
    }

    public function getProductNameAttribute()
    {
        return $this->category()->first()->name.' '.$this->variant()->first()->item()->first()->name.' '.$this->variant()->first()->size.' Kg';
    }

    public function scopeAvailableProducts($query){
        $available = DB::table('stock_transactions')
            ->select( 'products.id','products.price', DB::raw(' SUM(stock_transactions.pack_size) AS balancex '),
            DB::raw('CONCAT(product_categories.name, " ",items.name," ", item_variants.size," Kg") AS product_name'))
            ->join('products', 'stock_transactions.product_id', '=', 'products.id')
            ->join('product_categories', 'products.product_category_id', '=', 'product_categories.id')
            ->join('item_variants', 'products.item_variant_id', '=', 'item_variants.id')
            ->join('items', 'item_variants.item_id', '=', 'items.id')
            ->groupBy('products.id');
       return $available;
    }
}
