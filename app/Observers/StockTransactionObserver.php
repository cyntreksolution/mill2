<?php

namespace App\Observers;

use App\StockTransaction;

class StockTransactionObserver
{
    /**
     * Handle the stock transaction "created" event.
     *
     * @param \App\StockTransaction $stockTransaction
     * @return void
     */
    public function created(StockTransaction $stockTransaction)
    {
        if ($stockTransaction->type == 1) {
            $productBalance = $stockTransaction->product()->first()->balance;
            $stockpileBlance = $stockTransaction->intakeStockpile()->first()->balance;
            $bagBlance = $stockTransaction->product()->first()->bag()->first()->balance;
            $stockTransaction->product()->update(['balance' => $productBalance + $stockTransaction->pack_size]);
            $stockTransaction->intakeStockpile()->update(['balance' => $stockpileBlance + $stockTransaction->pack_size]);
            $stockTransaction->product()->first()->bag()->update(['balance' => $bagBlance - $stockTransaction->pack_size]);

        }else{
            $productBalance = $stockTransaction->product()->first()->balance;
            $stockpileBlance = $stockTransaction->intakeStockpile()->first()->balance;
            $stockTransaction->product()->update(['balance' => $productBalance + $stockTransaction->pack_size]);
            $stockTransaction->intakeStockpile()->update(['balance' => $stockpileBlance + $stockTransaction->pack_size]);

        }
    }

    /**
     * Handle the stock transaction "updated" event.
     *
     * @param \App\StockTransaction $stockTransaction
     * @return void
     */
    public function updated(StockTransaction $stockTransaction)
    {
        //
    }

    /**
     * Handle the stock transaction "deleted" event.
     *
     * @param \App\StockTransaction $stockTransaction
     * @return void
     */
    public function deleted(StockTransaction $stockTransaction)
    {
        //
    }

    /**
     * Handle the stock transaction "restored" event.
     *
     * @param \App\StockTransaction $stockTransaction
     * @return void
     */
    public function restored(StockTransaction $stockTransaction)
    {
        //
    }

    /**
     * Handle the stock transaction "force deleted" event.
     *
     * @param \App\StockTransaction $stockTransaction
     * @return void
     */
    public function forceDeleted(StockTransaction $stockTransaction)
    {
        //
    }
}
