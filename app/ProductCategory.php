<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    public function products(){
        return $this->hasMany(Product::class);
    }

    public function scopeAvailableProductsForCategory($query,ProductCategory $category){
        return $query
            ->join('products','products.product_category_id','=','product_categories.id')
            ->rightJoin('item_variants','products.item_variant_id','=','item_variants.id')
            ->where('products.item_variant_id','=',$category->id);
    }
}
