<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemVariant extends Model
{
    use SoftDeletes;
    protected $fillable=[
        'item_id',
        'size',
        'price'
    ];

    public function item(){
        return $this->belongsTo(Item::class);
    }

    public function products(){
        return $this->belongsToMany(ProductCategory::class,'products');
    }
}
