<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceData extends Model
{
    public function invoice(){
        return $this->belongsTo(InvoiceHeader::class,'invoice_header_id');
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
