<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BagTransaction extends Model
{
    public function bag(){
        return $this->belongsTo(Bag::class);
    }
}
