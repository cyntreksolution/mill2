<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stockpile extends Model
{
    public function getTypeAttribute($value){
        return ($value==1)?'Paddy':'Rice';
    }
}
