<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockTransaction extends Model
{
    public function getTypeAttribute($value)
    {
        return ($value == 1) ? 'loading' : 'unloading';
    }

    public static function batch()
    {
        $lastBatch = StockTransaction::latest()->first();
        if (!empty($lastBatch)) {
            return $lastBatch->batch + 1;
        } else {
            return 1;
        }

    }

    public function intakeStockpile()
    {
        return $this->belongsTo(IntakeStockpile::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public function scopeRiceLoading($query){
        return $query->whereType(1)
            ->whereNotNull('product_id')
            ->with('intakeStockpile.stockpile','product.variant.item','product.category');
    }

}
