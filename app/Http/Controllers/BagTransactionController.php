<?php

namespace App\Http\Controllers;

use App\BagTransaction;
use Illuminate\Http\Request;

class BagTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BagTransaction  $bagTransaction
     * @return \Illuminate\Http\Response
     */
    public function show(BagTransaction $bagTransaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BagTransaction  $bagTransaction
     * @return \Illuminate\Http\Response
     */
    public function edit(BagTransaction $bagTransaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BagTransaction  $bagTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BagTransaction $bagTransaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BagTransaction  $bagTransaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(BagTransaction $bagTransaction)
    {
        //
    }
}
