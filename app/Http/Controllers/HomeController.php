<?php

namespace App\Http\Controllers;

use App\InvoiceHeader;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('dashboard');
    }

    public function getMonthlySales()
    {
        $currentMonth = date('m');
        $data =InvoiceHeader::whereMonth('invoice_date','=',$currentMonth)->get()->sum('total');
        return response($data);
    }
}
