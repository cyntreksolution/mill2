<?php

namespace App\Http\Controllers;

use App\IntakeStockpile;
use App\Stockpile;
use Illuminate\Http\Request;

class StockpileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  view('stockpile.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stockpile  $stockpile
     * @return \Illuminate\Http\Response
     */
    public function show(Stockpile $stockpile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stockpile  $stockpile
     * @return \Illuminate\Http\Response
     */
    public function edit(Stockpile $stockpile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stockpile  $stockpile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stockpile $stockpile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stockpile  $stockpile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stockpile $stockpile)
    {
        //
    }

    public function getAllStockpiles($paginate=false){
        if ($paginate){
            return Stockpile::paginate(10);
        }else{
            $stockpiles = IntakeStockpile::with('stockpile')->get();
            return $this->sendResponse($stockpiles,'All Stockpiles');
        }

    }

    public function getAllRiceStockpiles($paginate=false){
        if ($paginate){
            return Stockpile::paginate(10);
        }else{

            $stockpiles = IntakeStockpile::whereHas('stockpile',function ($query){
                return $query->whereType(2);
            })->get();
            $items=[];
            foreach ($stockpiles as $stockpile){
                $item=[];
                $item['id']=$stockpile->id;
                $item['name'] =$stockpile->stockpile->name.' '.$stockpile->year.' '.$stockpile->intake;
                array_push($items,$item);
            }
            return $this->sendResponse(collect($items),'All Stockpiles');
        }

    }
}
