<?php

namespace App\Http\Controllers;

use App\Bag;
use App\BagTransaction;
use App\IntakeStockpile;
use App\ItemVariant;
use App\Product;
use App\ProductCategory;
use App\StockTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ProductCategory $category)
    {
        $item = ItemVariant::find($request->item_id);

        $productExists = Product::where('product_category_id', $category->id)->where('item_variant_id', $item->id)->first();

        if (empty($productExists)) {
            $product = new Product();
            $product->price = $request->price;
            $product->category()->associate($category);
            $product->variant()->associate($item);
            $product->save();
            return $this->sendResponse($product, 'Product Created Successfully');
        } else {
            return $this->sendError(null, 'Product Already Exist', 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {

    }

    public function getAllProducts($paginate = false)
    {
        if ($paginate) {

        } else {
            $products = Product::with('category', 'variant.item')->get();
            $items = [];
            foreach ($products as $product) {
                $item = [];
                $item['id'] = $product->id;
                $item['name'] = $product->category->name . ' ' . $product->variant->item->name . ' ' . $product->variant->size . ' KG';
                $item['price'] = $product->price;
                array_push($items, $item);
            }
            return $this->sendResponse(collect($items), 'All Products');
        }
    }


    public function sendRiceView()
    {
        return view('products.riceload');
    }

    public function loadProducts(Request $request)
    {
        $type = $request->type;
        $batch = StockTransaction::batch();
        if ($type == 'rice') {
            $items = $request->items;

            try {
                foreach ($items as $item) {
                    $product = Product::whereId($item['product']['id'])->with('variant','bag')->first();
                    $stockpile = IntakeStockpile::find($item['stockpile']['id']);
                    $packQuantity = $item['quantity'];

                    $unitPrice = $product->price;
                    $unitKg = $product->variant->size;
                    $kgQuantity = $unitKg * $packQuantity;
                    $value = $unitPrice * $packQuantity;
                    DB::transaction(function () use ($batch, $packQuantity, $kgQuantity, $product, $stockpile, $value) {
                        $stockTransaction = new StockTransaction();
                        $stockTransaction->type = 1;
                        $stockTransaction->batch = $batch;
                        $stockTransaction->pack_size = $packQuantity;
                        $stockTransaction->kg_size = $kgQuantity;
                        $stockTransaction->value = $value;
                        $stockTransaction->product()->associate($product);
                        $stockTransaction->intakeStockpile()->associate($stockpile);
                        $stockTransaction->save();

                        $bagTransaction = new BagTransaction();
                        $bag=Bag::whereId($product->bag->id)->first();
                        $bagTransaction->bag()->associate($bag);
                        $bagTransaction->quantity = -$packQuantity;
                        $bagTransaction->type =2;
                        $bagTransaction->save();

//                        $product->balance = $product->balance + $packQuantity;
//                        $product->save();
//
//                        $stockpile->balance = $stockpile->balance + $packQuantity;
//                        $stockpile->save();

                    });
                }
                return $this->sendResponse($product, 'Rice Loading completed');
            } catch (\Exception $exception) {
                return $this->sendError($exception, 'Sorry Something Went Wrong 1');
            }
        }
    }

    public function availableProducts($paginate =false){
        if ($paginate){

        }else{
            $available=Product::availableProducts()->get();
            return $this->sendResponse(collect($available), 'All Available Products');
        }
    }


}
