<?php

namespace App\Http\Controllers;

use App\ItemVariant;
use Illuminate\Http\Request;

class ItemVariantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $item = ItemVariant::firstOrCreate(
                ['item_id' => $request->item_id, 'size' => $request->size],
                ['price' => $request->price]
            );
            if ($item->wasRecentlyCreated) {
                return $this->sendResponse($item, 'Item Variant Created');
            } else {
                return $this->sendError(null, 'Item Variant Already Exist', 200);
            }
        } catch (\Exception $exception) {
            return $this->sendError($exception);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\ItemVariant $itemVariant
     * @return \Illuminate\Http\Response
     */
    public function show(ItemVariant $itemVariant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\ItemVariant $itemVariant
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemVariant $itemVariant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\ItemVariant $itemVariant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemVariant $itemVariant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\ItemVariant $itemVariant
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemVariant $itemVariant)
    {
        try {
            $itemVariant->delete();
            return $this->sendResponse((object)[], 'Item Variant Deleted');
        } catch (\Exception $exception) {
            return $this->sendError($exception);
        }
    }
}
