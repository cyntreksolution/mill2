<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param $result
     * @param $message
     * @return JsonResponse
     */
    public function sendResponse(object $result, string $message)
    {
        $response = [
            'status' => 'success',
            'data' => $result,
            'message' => $message,
        ];

        return response()->json($response, 200);
    }

    /**
     * @param \Exception $exception
     * @param string $message
     * @param int $code
     * @return array
     */
    public function sendError(\Exception $exception = null,string $message = null,$code = 500)
    {
        $data = [];

        if (App::environment('local')) {
            if (!empty($exception)) {
                $data = [
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
//                    'exception' => (string)$exception,
                    'line' => $exception->getLine(),
                    'file' => $exception->getFile()
                ];
                $code = $exception->getCode();
            }
        } else {
            $data['message'] = 'server error production';
        }

        $response = [
            'status' => 'failed',
            'message' => !empty($data)?$data:$message,
        ];

        return response()->json($response, $code);
    }
}
