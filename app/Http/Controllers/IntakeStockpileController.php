<?php

namespace App\Http\Controllers;

use App\IntakeStockpile;
use Illuminate\Http\Request;

class IntakeStockpileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type = $request->type;
        if ($type == 'rice') {
            return view('stockpile.rice');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\IntakeStockpile $intakeStockpile
     * @return \Illuminate\Http\Response
     */
    public function show(IntakeStockpile $intakeStockpile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\IntakeStockpile $intakeStockpile
     * @return \Illuminate\Http\Response
     */
    public function edit(IntakeStockpile $intakeStockpile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\IntakeStockpile $intakeStockpile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, IntakeStockpile $intakeStockpile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\IntakeStockpile $intakeStockpile
     * @return \Illuminate\Http\Response
     */
    public function destroy(IntakeStockpile $intakeStockpile)
    {
        //
    }

    public function getAllIntakeStockpiles()
    {
        return IntakeStockpile::with('stockpile')->paginate(10);
    }
}
