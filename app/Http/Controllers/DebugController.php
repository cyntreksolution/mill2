<?php

namespace App\Http\Controllers;

use App\InvoiceHeader;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Storage;
use Response;

class DebugController extends Controller
{
    public function index()
    {
        $filename = $this->generateInvoicePdf(13);
        return $this->getPDfInvoice($filename, 'download');
    }

    public function generateInvoicePdf($invoiceId)
    {
        $invoice = InvoiceHeader::whereId($invoiceId)->with('items.product.variant.item', 'client', 'driver')->first();
        $pdf = PDF::loadView('invoice.invoice', compact('invoice'))->setPaper('a4', 'portrait');
        $filename = $invoice->invoice_number . '.pdf';
        Storage::disk('local')->put('invoices/' . $filename, $pdf->output());
        return $filename;
    }

    public function getPDfInvoice($filename, $action = 'stream')
    {
        if ($action == 'stream') {
            return Storage::response('invoices/' . $filename);
        } elseif ($action == 'download') {
            return Storage::download('invoices/' . $filename);
        }
    }
}
