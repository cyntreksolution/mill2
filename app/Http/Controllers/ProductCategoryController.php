<?php

namespace App\Http\Controllers;

use App\ItemVariant;
use App\ProductCategory;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $category = ProductCategory::whereName($request->name)->first();
            if (empty($category)) {
                $category = new ProductCategory();
                $category->name = $request->name;
                $category->save();
                if ($category) {
                    return $this->sendResponse($category, 'Product Category Created');
                }
            } else {
                return $this->sendError(null, 'Product Category Already Exist', 200);
            }
        } catch (\Exception $exception) {
            return $this->sendError($exception);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\ProductCategory $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $category)
    {
        $category = ProductCategory::whereId($category->id)->with('products.variant.item')->first();
        return $this->sendResponse($category, 'Category Found');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\ProductCategory $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCategory $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\ProductCategory $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductCategory $category)
    {
        try {
            $category->name = $request->name;
            $category->save();
            if ($category->wasChanged()) {
                return $this->sendResponse($category, 'Category Updated');
            } else {
                return $this->sendError(null, 'No Changes Done', 200);
            }

        } catch (\Exception $exception) {
            return $this->sendError($exception);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\ProductCategory $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductCategory $category)
    {
        try {
            $category->delete();
            return $this->sendResponse((object)[], 'Category Deleted');
        } catch (\Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function getAllCategories($paginate = false)
    {
        $categories = ($paginate) ? ProductCategory::with('products.variant.item')->orderBy('name')->paginate($paginate) : ProductCategory::with('products.variant.item')->orderBy('name')->get();
        return $this->sendResponse($categories, 'category list');
    }

    public function availableItems(Request $request, ProductCategory $category)
    {
        $available = ItemVariant::whereDoesntHave('products', function ($query) use ($category) {
            $query->where('products.product_category_id', $category->id);
        })->with('item')->get();
        return $this->sendResponse($available, 'Available Items');
    }
}
