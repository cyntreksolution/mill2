<?php

namespace App\Http\Controllers;

use App\Client;
use App\Driver;
use App\InvoiceData;
use App\InvoiceHeader;
use App\Product;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use http\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class InvoiceController extends Controller
{
    public function index()
    {
        return view('invoice.invoice-list');
    }

    public function create(){
        return view('invoice.index');
    }

    public function store(Request $request)
    {
        $items = $request->items;
        $invoice = $request->invoice;
        $action = (!empty($request->pdf)) ? $request->pdf : false;
        $client = Client::whereId($invoice['client']['id'])->first();

        $driver = Driver::firstOrCreate(
            ['name' => $invoice['driver']['name']]
        );
        $filename = null;
        $invoice = $this->makeInvoice($invoice, $driver, $client, $items);
        if ($invoice) {
            $filename = $this->generateInvoicePdf($invoice->id);
            if ($action) {
                return url('invoice/'.$filename.'/'.$action);
            } else {
                return $this->sendResponse($invoice, 'Invoice Created Successfully');
            }

        } else {
            return $this->sendError(null, 'invoice not generated');
        }

    }

    public function makeInvoice($invoice, $driver, $client, $items)
    {
        try {
            $createdInvoice = 12;
            DB::transaction(function () use ($invoice, $driver, $client, $items,&$createdInvoice) {
                $invoice_header = new InvoiceHeader();
                $invoice_header->invoice_number = InvoiceHeader::invoiceNo();
                $invoice_header->store_id = $invoice['store']['id'];
                $invoice_header->invoice_date = Carbon::now()->format('Y-m-d');
                $invoice_header->due_date = Carbon::now()->addWeek()->format('Y-m-d');
                $invoice_header->total = 0;
                $invoice_header->driver()->associate($driver);
                $invoice_header->client()->associate($client);
                $invoice_header->save();

                $invoiceTotal = 0;
                foreach ($items as $item) {
                    $product = Product::whereId($item['product']['id'])->first();

                    $qty = $item['quantity'];
                    $rate = $item['rate'];
                    $total = $qty * $rate;

                    $invoice_data = new InvoiceData();
                    $invoice_data->invoice()->associate($invoice_header);
                    $invoice_data->product()->associate($product);
                    $invoice_data->quantity = $qty;
                    $invoice_data->rate = $rate;
                    $invoice_data->total = $total;
                    $invoiceTotal = $invoiceTotal + $total;
                    $invoice_data->save();
                }

                $invoice_header->total = $invoiceTotal;
                $invoice_header->save();
                $createdInvoice = $invoice_header;
            });
            return $createdInvoice;
        } catch (\Exception $exception) {
            return false;
        }
    }

    public function generateInvoicePdf($invoiceId)
    {
        $invoice = InvoiceHeader::whereId($invoiceId)->with('items.product.variant.item', 'client', 'driver')->first();
        $pdf = PDF::loadView('invoice.invoice', compact('invoice'))->setPaper('a4', 'portrait');
        $filename = $invoice->invoice_number . '.pdf';
        Storage::disk('local')->put('invoices/' . $filename, $pdf->output());
        return $filename;
    }

    public function getPDfInvoice($filename, $action = 'stream')
    {
        if (Storage::disk('local')->exists( 'invoices/' . $filename )){
            if ($action == 'stream') {
                return Storage::response('invoices/' . $filename);
            } elseif ($action == 'download') {
                return Storage::download('invoices/' . $filename);
            }
        }else{
            $invoice_no = str_replace('.pdf','',$filename);
            $invoice_id =InvoiceHeader::whereInvoiceNumber($invoice_no)->first()->id;
            $this->generateInvoicePdf($invoice_id);
            return $this->getPDfInvoice($filename,$action);
        }

    }

    public function pdfRequestLink(Request $request){
        $filename= $request->filename;
        $action=$request->action;
        return $this->getPDfInvoice($filename,$action);
    }

    public function getAllInvoices(Request $request){
        if ($request->page){
            return InvoiceHeader::orderBy('status','DESC')->latest()->paginate(10);
        }else{

        }
    }
}
