<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('items.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $item = Item::firstOrCreate(
                ['name' => $request->name],
                ['price' => $request->price, 'type' => $request->type]
            );
            if ($item->wasRecentlyCreated) {
                return $this->sendResponse($item, 'Product Created');
            } else {
                return $this->sendError(null, 'Product Already Exist', 200);
            }
        } catch (\Exception $exception) {
            return $this->sendError($exception);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        $item=Item::whereId($item->id)->with('variants')->first();
        return $this->sendResponse($item, 'Product Found');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        try {
            $item->name = $request->name;
            $item->price = $request->price;
            $item->type = $request->type;
            $item->save();
            if ($item->wasChanged()) {
                return $this->sendResponse($item, 'Product Updated');
            } else {
                return $this->sendError(null, 'No Changes Done', 200);
            }

        } catch (\Exception $exception) {
            return $this->sendError($exception);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Item $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        try {
            $item->delete();
            return $this->sendResponse((object)[], 'Product Deleted');
        } catch (\Exception $exception) {
            return $this->sendError($exception);
        }
    }

    public function getAllProducts($paginate = false)
    {
        $products = ($paginate) ? Item::with('variants')->orderBy('name')->paginate($paginate) : Item::with('variants')->orderBy('name')->get();
        return $this->sendResponse($products, 'items list');
    }
}
