<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;

    protected $fillable = ['name','price','type'];

    public function setTypeAttribute($value)
    {
        $this->attributes['type'] =  ($value=='Paddy')?1:2;
    }

    public function getTypeAttribute($value)
    {
        return ($value==1)?'Paddy':'Rice';
    }

    public function getPriceAttribute($value)
    {
        return round($value,2);
    }

    public function variants(){
        return $this->hasMany(ItemVariant::class);
    }
}
