<?php

namespace App\Providers;

use App\Observers\ProductObserver;
use App\Observers\StockTransactionObserver;
use App\Product;
use App\StockTransaction;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Product::observe(ProductObserver::class);
        StockTransaction::observe(StockTransactionObserver::class);
    }
}
