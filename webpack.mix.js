const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');
mix.js('resources/js/notifications.js', 'public/js');
mix.js('node_modules/sweetalert2/dist/sweetalert2.all.js', 'public/js');
mix.js('resources/css/bootstrap-switch.js', 'public/black/js/plugins');
mix.js('resources/css/bootstrap-tagsinput.js', 'public/black/js/plugins');
mix.copy('resources/css/invoice.css', 'public/css/invoice.css');
mix.copy('resources/css/bootstrap.min.css', 'public/css/bootstrap.min.css');
