<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_headers', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('invoice_number')->unique();
            $table->integer('client_id');
            $table->integer('store_id');
            $table->integer('driver_id');
            $table->date('invoice_date');
            $table->date('due_date');
            $table->double('total');
            $table->tinyInteger('status')->default(3);//1 paid by cash,2 paid by cheque,3 pending,4 outdated
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_headers');
    }
}
