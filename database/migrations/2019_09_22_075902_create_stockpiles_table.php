<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockpilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stockpiles', function (Blueprint $table) {
            $table->integer('id',true);
            $table->string('name',200);
            $table->text('address')->nullable();
            $table->string('telephone',10)->nullable();
            $table->string('mobile',10)->nullable();
            $table->tinyInteger('type')->default(1);//1 paddy 2 rice
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stockpiles');
    }
}
