<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntakeStockpilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intake_stockpiles', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('stockpile_id');
            $table->integer('year')->nullable();
            $table->string('intake',200)->nullable();
            $table->double('balance')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('intake_stockpiles');
    }
}
