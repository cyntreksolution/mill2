<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBagTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bag_transactions', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('bag_id');
            $table->integer('quantity');
            $table->integer('type')->default(1);// 1 = bag store 2 = packing
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bag_transactions');
    }
}
