<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_transactions', function (Blueprint $table) {
            $table->integer('id',true);
            $table->integer('intake_stockpile_id');
            $table->integer('product_id')->nullable();//if paddy item id
            $table->integer('item_id')->nullable();//if paddy item id
            $table->integer('type')->default(1);//1 = loading 2 = unloading
            $table->integer('batch')->default(0);
            $table->double('pack_size')->nullable();
            $table->double('kg_size')->nullable();
            $table->double('value')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_transactions');
    }
}
